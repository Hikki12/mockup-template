
#include <ArduinoJson.h>
#include <SimpleTimer.h>


/*
Events:
  Recived:
    $stop: it will stop experiment
    $status: it asks for the current state
  Emitted: 
    $recived -> it notifies the data were recived
    $status, ok -> response status of the system is ok
    $status, error:msg  -> response system has some error
*/

/* PINS AREA  --------------------------------------------------------------------------------*/

const int pin1 = 4;
const int pin2 = 5;

const int lightPin = 6;

/* VARIABLES FOR SERIAL ---------------------------------------------------------------------*/

String inputString = "";         // a String to hold incoming data
String event = "";
bool stringComplete = false;  // whether the string is complete

/* VARIABLES AREA ----------------------------------------------------------------------------*/

// Initialize JSON object *************
StaticJsonvariablesument<512> variables;

// JSON variables {

bool start = false;
bool lightOn = false;

float var3 = 3.211;
long var4 = 21111;

// }

// Auxiliar Variables *****************
// These variables will be helpful for monitoring the mockup use
bool isInUse = false;

// Internal Variables *****************

bool speedReady = true;


/* TIMERS AREA --------------------------------------------------------------------------------*/

// ****** STOP TIMER ******
const long stopInterval = 60000; //ms
const int maxStopMinutes = 10; 
int stopMinutes = 0;

SimpleTimer stopTimer(stopInterval);

void autoStop(){

  if(stopTimer.isReady()){
    stopMinutes++;
    stopTimer.reset();
  }

  if(stopMinutes >= maxStopMinutes){
    stop();
    stopMinutes = 0;
  }

}

void resetStopTimer(){
  stopMinutes = 0;
  stopTimer.reset();
}
// **************************


/* SEND/READ INFO -----------------------------------------------------------------------------*/

void sendVariables(){
  variables["start"] = start;
  variables["lightOn"] = lightOn;

  variables["var3"] = var3;
  variables["var4"] = var4;
  serializeJson(variables, Serial);  
  Serial.println();
}

void dataRecived(){
  // Notify I have recived the data
  Serial.println("$recived");
}

void readVariables(){
  
  DeserializationError error = deserializeJson(variables, inputString);
  
  if(error){
    // Serial.println(error.f_str());
    return;
  }
  // Set Variables
  // start = variables["start"];
  // lightOn = variables["lightOn"];

  // var3 = variables["var3"];
  // var4 = variables["var4"];

  dataRecived();

  configureExperiment();

}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char) Serial.read();
    inputString += inChar;
    
    if (inChar == '\n') {
      
      if(inputString[0] == '$'){
        readEvents();
      }
      
      if(inputString[0] == "{"){
        readVariables();
      }
      
      stringComplete = true;
      inputString = "";
      event = "";
    }
  }
}

void readEvents(){

    event = inputString.substring(1);
    event.trim();

    if(event == "stop"){
      stop();
      return;
    }

    if(event == "status"){
      areYouOk();
      return;
    }
}

/* CHECK SYSTEM ---------------------------------------------------------------------------------*/
void areYouOk(){
  bool ok = true;
  String error = "";
  
  // check if are there any error on the system
  if(ok){
    Serial.println("$status,ok");
  }else{
    Serial.println("$status,error: " + error);
  }
}

/* SYSTEM FUNCTIONS -----------------------------------------------------------------------------*/
// Add your logic here!

void configureExperiment(){
  isInUse = true;
  resetStopTimer();
}

void stop(){
  start = false;
  lightOn = false;
  sendVariables();

  isInUse=true;
  resetStopTimer();
  //Serial.println("$stopped");
}

void lightControl(){
  digitalWrite(lightPin, lightOn);
}

void motorControl(){
  //
}

void sensorReading(){
  //
}

/* SET UP -----------------------------------------------------------------------------------------*/

void configurePins(){
  pinMode(OUTPUT, pin1);
  pinMode(OUTPUT, pin2);
  pinMode(OUTPUT, lightPin);
}

void setup() {
  Serial.begin(9600);
    
  inputString.reserve(256);
  event.reserve(256);

  configurePins();
}

void loop() {
  autoStop();
  lightControl();
}