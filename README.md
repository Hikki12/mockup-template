# Python-mockup-template

This is an example for a python mockup.

# Description

Add some description here...

# Project Estructure
The project has three principal folders:
```
|-- arduino
|       |-- template.ino
|-- src
|   |-- main.py
|   |-- options.py
|   |-- camera_utils.py
|-- python-mockupio
|-- .gitignore
|-- .gitmodules
|-- configure.sh
|-- README.md
|-- run.sh
|-- update.sh
```
**src** folder has your code, here you have a template, you should edit and add what you want.
<br/>
**arduino** folder has the arduino code template.
<br/>
**python-mokupio** is the base library.

# Configuration

First of all clone this repository. After you should run the bash script to auto-configure your enviroment work. Don't forget to give access to the file:
## Auto
```
# Give access to the file
sudo chmod u+x configure.sh

# Excute the file
. ./configure.sh
```
## Manual

If something were wrong or if you prefer, open your console on mockup-template directory and do this: 
```
# Create a virtualenv
python3 -m venv venv_name 

# Load virtualenv
source venv_name/bin/activate

# Entry to mockupio library directory
cd python-mockupio

# Download mockupio library
git submodule update --init --recursive --force

# Return to the main folder
cd ..
```

# Run

You should run the code with the virtualenv:

```
# Set src directory
cd src/

# Run the script
python main.py
```

# Options

You could configure some parameters, this configurations are on **src/options.py** file:

## Serial

```
serial_options = {
    "port": None, # a str name, if you give None it will connect to the first device founded.
    "baudrate": 9600, # give a baudrate
    "timeout": 0.25, # seconds
    "reconnection_time_serial": 1 # seconds
}
```

## Camera

```
camera_options = {
    "index_device": 0,  # Index camera device
    "size": (320, 240),  # Image size
    "quality": 50,  # jpeg quality
    "fps_record": 10, # fps speed of capture
    "h_flip": True, # flip horizontally?
    "v_flip": False # flip vertically?

}
```

## Socket-io

```
socketio_options = {
    "server_address": "http://your.domain.com", # Change this with your server address
    "identifier": "MAQUETA-FREEFALL", # an ID
    "fps_stream": 7, # fps speed of streaming
    "reconnection_time_socketio": 2, # seconds
    "auto_stream": True, # If it's True it will control streaming of video automatically, else it will be ever streaming
    "stream_to_host": False, # if it's True it will stream directlly to the server address, else it will autoadmin streaming namespace
    "latency": 0.005, # latency in seconds
    "write_delay": 0.33 # seconds
}

```

## Extra

```
extra_options = {
    "multiprocess_camera": True, # if it's True, a new process will be create to manage the Camera
    "multiprocess_serial": False, # if it's True, a new process will be create to manage the Serial
    "multiprocess_socketio": False, # if it's True, a new process will be create to manage the Socketio
    "debug_camera": True, # to enable logger of camera
    "debug_serial": True, # to enable logger of serial
    "debug_socketio": True # to enable logger of socketio
}

```

# Coding

```
from mockupio import MockupClient

client = MockupClient(**camera_options, **serial_options, **socketio_options, **extra_options)
```

# Events
Available events are:

```
# Camera events
client.on('camera-image-ready', show_image)

# Serial events
client.on('serial-connection-event', handle_serial_connection_status)
client.on('serial-incoming-data', recive_data_serial)

# Socket-io events
client.on('socketio-connection-event', handle_socketio_connection_status)
client.on('socketio-request-updates', response_to_server)
client.on('socketio-incoming-data', recive_data_socketio)
client.on('socketio-stop-event', handle_stop_event_socketio)
client.on('socketio-busy', handle_busy_event)

# Errors
client.on('error', handle_errors)

```

Callbacks looks like:
```

def show_image(variables, frame, results):
    print("Showing frame...", type(frame), type(results))

def record_end(self):
    print("Recording camera ended!")

def handle_serial_connection_status(variables, status):
    print("Serial connection status: ", status)

def recive_data_serial(variables, data):
    print("Serial says: ", data)    
    if isinstance(data, str):
        if data[0] != "$":
            try:
                variables = json.loads(data)
                print(variables)
            except Exception as e:
                print(e)
        else:
            print("event: ", data)

def handle_stop_event_socketio():
    print("stoping mockup")
    self.client.write_to_serial("$stop", toJson=False)

def handle_socketio_connection_status(status):
    print("socketio connection status: ", status) 

def handle_busy_event(status):
    print("Is busy?: ", status)

def response_to_server():
    print("Responding to server ...")

def recive_data_socketio(data):
    print("Socketio recives: ", data)
    client.write(data, toSerial=True, toServer=False) 

def handle_errors(error):
    print(error)
```

# Variables write
For write variables try something like this:

```
#  dict
variables = {
    'var1': True,
    'var2': 2.45,
    'var3': 7
}

# Write at the same time to the serial device and to the server.
client.write(variables, toSerial=True, toServer=True, toJson=True)

# You can also write the same like this.
client.write_to_serial(variables, toJson=True)
client.write_to_server(variables)
```
You could write message to serial as follows:
```
message = 'my-message'
# Disable JSON conversion
client.write_to_serial(message, toJson=False)
```

# Especial Functions
Some functions can be added to the camera image obtain process. The camera process runs as follows:
```
"""Mockup-io source code"""

# Camera image obtain process

image = camera.read()

# Optional preprocess function | 'preprocess'
image = preprocess(image)

# Optional image processing function | 'after-read'
results = image_process(image)

```
If you have some results they will be added on the camera-image-ready event callback:
```
def show_image(image, results):
    if results is not None:
        print("Your results are here: ", results)

client.on('camera-image-ready', show_image)
```
## Camera - preprocess
This function will be called after camera returns a frame.
```
def preprocess(img): 
    img = img / 255
    return img

client.camera.set_callback('preprocess', preprocess)
```
## Camera - after read
This Function will be executed after preprocess. 
```
def image_processing(img, *args):
    # Read multiprocess variables
    image_process_vars = args[0]
    color = image_process_vars["color"]

    # Apply some processing code
    if color == "orange":
        print("Tracking orange color.")
    if color == "yellow":
        print("Tracking yellow color.")
    x, y = 0, 0
    position = (x, y)
    # This result will be added to results function
    return  position

# Create multiprocess variables like a dict
image_process_vars = client.obtain_dict()
image_process_vars["color"] = "orange"

# Set process function with multiprocess variables
client.camera.set_callback('after-read', image_processing, image_process_vars)
```



# Arduino

You could use the Arduino template include in the folder **arduino**.

# Updates

To check for updates of the python mockup-library do:

```
# Give acces
sudo chmod u+x update.sh

# Run
./update.sh
```

# Create a repository

If you want to create a custom repository to your project, you could do like this:

```
git remote add origin https://your-repo.git
```

# License

MIT
s