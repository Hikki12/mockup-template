import cv2 
import numpy as np


def image_adjust(img, a=0.8, b=1, sat=1.1):
    """It adjust contrast, brightness and saturation of a image
    :param img: image array
    :param a: contrast
    :param b: brightness
    :param sat: saturation
    :return: img: image array processed
    """
    img = cv2.convertScaleAbs(img, alpha=a, beta=b)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV).astype("float32")
    h, s, v = cv2.split(hsv)
    s = s * sat
    s = np.clip(s, 0, 255)
    hsv = cv2.merge([h, s, v])
    img = cv2.cvtColor(hsv.astype("uint8"), cv2.COLOR_HSV2BGR)
    return img


def crop_image(img, xratio=0.7, yratio=1):
    """It crops the image
    :param img: image array
    :param xratio
    :param yratio
    """
    center = np.array(img.shape)/2
    x = int(center[1])
    y = int(center[0])
    a = x - int(xratio*x)
    b = x + int(xratio*x)
    c = y - int(yratio*y)
    d = y + int(yratio*y)
    new = img[c:d, a:b]
    return new


def preprocess(img, **kwargs):
    """It preprocess the image
    :param img: image array
    """
    img = image_adjust(img, a=0.8,b=1, sat=1.3)
    #img = crop_image(img)
    return img
